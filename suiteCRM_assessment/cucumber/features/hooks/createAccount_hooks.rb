#Criar o objeto e setar as variaveis para os campos
Before '@hooks_createAccount' do
  @CreateAccount = CreateAccount.new #Objeto página Login
  @name = Faker::Name.name #nome a ser criado
  @phone = Faker::PhoneNumber.phone_number #=> "397.693.1309"
  @website = "http://www.test.com"
  @fax_number = Faker::Number.number(10)
  @email = Faker::Internet.email
  @fieldBuAddress_street = Faker::Address.street_name #=> "Larkin Fork"
  @fieldShiAddress_street = Faker::Address.street_name
  @fieldBuaddress_city = Faker::Address.city #=> "Imogeneborough"
  @shipping_address_city = Faker::Address.secondary_address
  @billing_address_state = Faker::Address.state #=> "California"
  @billing_address_postalcode = Faker::Address.zip_code #=> "58517" or "23285-4905"
  @shipping_address_postalcode = Faker::Address.zip_code
  @billing_address_country = Faker::Address.country #=> "French Guiana"
  @shipping_address_country = Faker::Address.country_code #=> "IT"
  @fieldDescription = Faker::Lorem.characters(300)
  @assigned_user_name = 'Will Westin'
  @account_type = "Test"
  @industry = Faker::Company.bs
  @annual_revenue = "Davenport Investing"
  @employees = Faker::Job.title
  @parent_name = "B.H. Edwards Inc"
  @campaign_name = Faker::Job.field




end
