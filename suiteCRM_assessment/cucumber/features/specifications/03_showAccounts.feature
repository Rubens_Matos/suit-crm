#language: pt
@hooks_accountExistent
Funcionalidade: Exibir todas as contas cadastradas na aplicação
 Como usuário da aplicação, desejo exibir as contas
 criadas e vizualizar essas contas dos usuários cadastrados

 Cenario: Exibir as contas cadastradas
  Dado que eu estou logado na aplicação
  Quando posicionar o mouse na opção de menu 'SALES'
  E clicar na opção de subMenu 'Accounts'
  Então o sistema exibe as contas cadastradas na aplicação
