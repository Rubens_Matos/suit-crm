#language: pt
@hooks_createAccount
Funcionalidade: Criar conta de usuário para Funcionalidades do portal 'suiteondemand.com'

Como usuário da aplicação, desejo criar no sistema
uma conta para o usuário realize as operações no sistema de acordo seu perfil

Cenario: Criar conta de usuário

    Dado que estou logado no sistema
    Quando passar o mouse na opção de menu 'ACCOUNTS'
    E selecionar no submenu 'Create Account'
    E informar todos os campos da interface 'CREATE'
    E acionar o comando 'SAVE'
    Entao exibe os detalhes da conta criada
