#language: pt
@hooks_login
Funcionalidade: Acessar a aplicação de avaliação conta.Mobi

Como usuário da aplicação, desejo logar no sistema
para criar e vizualizar as contas dos usuários cadastrados

Cenario: Login com sucesso

    Dado que eu acesse a página do Suite CRM
    Quando clico no link 'Log in to Demo'
    E eu informar 'Login' e 'senha'
    Entao o exibe o nome do usuário logado
