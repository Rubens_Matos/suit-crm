#mapa elementos da página create account
class CreateAccount < SitePrism::Page
  element :cmdCreate, :xpath, "/html/body/div[2]/nav/div/div[5]/ul/li[1]/a" #Comando CREATE
  element :fieldName, "#name"
  element :fieldOfficePhone, "#phone_office"
  element :fieldWebSite, "#website"
  element :fieldPhone_fax, "#phone_fax"
  element :fieldAccountEmail, "#Accounts0emailAddress0"
  element :fieldBuAddress_street, "#billing_address_street"
  element :fieldShiAddress_street, "#shipping_address_street"
  element :fieldBuaddress_city, "#billing_address_city"
  element :shipping_address_city, "#shipping_address_city"
  element :billing_address_state, "#billing_address_state"
  element :shipping_address_state, "#shipping_address_state"
  element :billing_address_postalcode, "#billing_address_postalcode"
  element :shipping_address_postalcode, "#shipping_address_postalcode"
  element :billing_address_country, "#billing_address_country"
  element :shipping_address_country, "#shipping_address_country"
  element :fieldDescription, "#description"
  element :assigned_user_name, "#assigned_user_name" #= Will Westin
  element :account_type, "#account_type" #select
  element :industry, "#industry"#select
  element :annual_revenue, "#annual_revenue" #= Davenport Investing
  element :employees_fist, "#employees"
  element :parent_name, "#parent_name" # = B.H. Edwards Inc
  element :employees, "#employees"
  element :campaign_name, "#campaign_name"
  element :cmdSave, :xpath, "(//input[@id='SAVE'])[2]"
end
