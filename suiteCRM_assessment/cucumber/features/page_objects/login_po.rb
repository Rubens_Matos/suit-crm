class PaginaLogin < SitePrism::Page
  element :fieldLogin, :id, "user_name" #Campo de Login
  element :fieldSenha, :id, "username_password" #campo de senha
  element :btnLogin, :id, "bigbutton" #botão logar
end
