class PaginaShowAccount < SitePrism::Page #objetos da page exibir contas
  element :menuAccount, "#moduleTab_Accounts"
  element :subMenuAccount, :xpath, "//div[@id='toolbar']/ul/li[2]/ul/li/ul/li[2]/a/span[2]"
  element :backTohome, :xpath, "//a[contains(text(),'SuiteCRM')]"
end
