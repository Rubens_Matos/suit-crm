#Dumisani Mhlophe
Dado(/^que estou logado no sistema$/) do
   page.evaluate_script('window.history.back()')#atualizar a página

   expect(page).to have_content 'Will Westin' #validar  login na aplicação pelo usuário logado

end

Quando(/^passar o mouse na opção de menu 'ACCOUNTS'$/) do
  page.find(:xpath,'/html/body/div[2]/nav/div/div[5]/ul/li[1]/a').hover #mouseOver no menu para acessar o sub menu

end

Quando(/^selecionar no submenu 'Create Account'$/) do
  page.find(:xpath, "/html/body/div[2]/nav/div/div[5]/ul/li[1]/ul/li[1]/a").click #Clicar na opção de subMenu
end

Quando(/^informar todos os campos da interface 'CREATE'$/) do
  @CreateAccount.fieldName.set @name #preencher o campo nome
  @CreateAccount.fieldOfficePhone.set @phone #preencher o campo Office Phone
  @CreateAccount.fieldWebSite.set @website #preencher o campo website
  @CreateAccount.fieldPhone_fax.set @fax_number #preencher o campo fax
  @CreateAccount.fieldAccountEmail.set @email #preencher o campo Email Address
  @CreateAccount.fieldBuAddress_street.set @fieldBuAddress_street #preencher o campo street
  @CreateAccount.fieldShiAddress_street.set @fieldShiAddress_street #preencher o campo street
  @CreateAccount.fieldBuaddress_city.set @fieldBuaddress_city #preencher o campo city
  @CreateAccount.shipping_address_city.set @shipping_address_city #preencher o campo city
  @CreateAccount.billing_address_state.set @billing_address_state #preencher o campo state
  @CreateAccount.shipping_address_state.set @billing_address_state #preencher o campo
  @CreateAccount.billing_address_postalcode.set @billing_address_postalcode #preencher o campo postalcode
  @CreateAccount.shipping_address_postalcode.set @shipping_address_postalcode #preencher o campo postalcode
  @CreateAccount.billing_address_country.set @billing_address_country #preencher o campo country
  @CreateAccount.shipping_address_country.set @shipping_address_country #preencher o campo country
  @CreateAccount.fieldDescription.set @fieldDescription #preencher o campo description
  @CreateAccount.assigned_user_name.set @assigned_user_name #preencher o campo Assigned to
  @CreateAccount.account_type.select("Analyst") #preencher o campo account type
  @CreateAccount.industry.select("Banking") #preencher o campo industry
  @CreateAccount.annual_revenue.set @annual_revenue #preencher o campo Annual Revenue
  @CreateAccount.employees.set @employees #preencher o campo Employees
  @CreateAccount.parent_name.set @parent_name #preencher o campo Member of
  @CreateAccount.campaign_name.set @campaign_name #preencher o campo Campaign
end

Quando(/^acionar o comando 'SAVE'$/) do
  @CreateAccount.cmdSave.click #clicar comando Salvar
end

Entao(/^exibe os detalhes da conta criada$/) do
  name = page.find("#pagecontent").text #Expect nome de conta criada
  @name.upcase
  expect(name).to eq name
end
