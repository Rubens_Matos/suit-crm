Dado(/^que eu acesse a página do Suite CRM$/) do
  visit "/" #acessa página Root
end
Quando(/^clico no link 'Log in to Demo'$/) do
  click_link 'Log in to Demo' #clicar no comando para acessar pagina de login
end

Quando(/^eu informar 'Login' e 'senha'$/) do
  @Login.fieldLogin.set @login #informar o login do usuário
  @Login.fieldSenha.set @senha #informar a senha
  @Login.btnLogin.click #Clicar no comando para logar na aplicação
end
Entao(/^o exibe o nome do usuário logado$/) do
  user = page.find("#with-label").text #guardar o nome do usuário da conta
  expect(user).to eq user #validar nome da conta/usuário criado
  sleep 3
  expect(page).to have_selector(:xpath, '/html/body/div[3]/div/div/div[1]')
end
