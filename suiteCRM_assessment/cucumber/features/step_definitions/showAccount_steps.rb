Dado(/^que eu estou logado na aplicação$/) do
   page.evaluate_script('window.history.back()')#atualizar a página
end

Quando(/^posicionar o mouse na opção de menu 'SALES'$/) do
    @ShowAccounts.menuAccount.hover #posiocionar mouse sobre menu "SALES"
end

Quando(/^clicar na opção de subMenu 'Accounts'$/) do
  @ShowAccounts.subMenuAccount.click #Clicar na opção de subMenu para exibir as contas
end

Então(/^o sistema exibe as contas cadastradas na aplicação$/) do
  expect(page).to have_xpath("//a[contains(.,'Accounts')]") #validar se está na página de exibição das contas
end
