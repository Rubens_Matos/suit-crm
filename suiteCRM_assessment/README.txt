Quality Assurance Engineer - Rubens Ribeiro de Matos
Testes funcionais sobre a plataforma suite CRM. Os testes foram feitos com Cucumber e Capybara com o conceito de Page Objects.

*O projeto est� estruturado da seguinte forma:
  cucumber
   features
     hooks
     page_objects
     specifications
     step_definitions
     support
     screenshots

*Driver necess�rio:
 Por padr�o os testes ser�o executados no Chrome.
  Instale o Nodejs (Next, Next e Finish) https://nodejs.org/en/ e depois o comando abaixo:
    npm install -g chromedriver

*Configurando o ambiente:
  Necess�rio ruby 2.3.3p222.
  Instalando o bundler. Navegue na pasta \suiteCRM_assessment e execute o seguinte comando:
    gem install bundler

*Instalando as gems:
  Execute o seguinte comando dentro da raiz do projeto:
     bundle install
  Execute o comando "bundle install" na pasta suiteCRM_assessment.

*Executando os testes:
 Na pasta suiteCRM_assessment\cucumber, execute o comando abaixo para execu��o de todos os testes:
    cucumber

*Relat�rio dos testes em HTML:
  Ap�s a execu��o dos testes estar� dispon�vel um relat�rio na pasta suiteCRM_assessment\cucumber\features_report.html
  obs.: Os screenshots estar�o dispon�veis no p� de cada funcionalidade do relat�rio.